const express = require('express')

const app = express()

app.get('/', (req, res) => res.json(Date.now()))

app.listen(4000)
